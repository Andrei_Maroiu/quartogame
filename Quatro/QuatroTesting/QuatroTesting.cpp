#include "pch.h"
#include "CppUnitTest.h"
#include "../Quatro/Piece.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuatroTestingNamespace
{
	TEST_CLASS(QuatroTestingClass)
	{
	public:
		
		TEST_METHOD(TestMethod1)
		{
		}
	};

	TEST_CLASS(QuatroPieceTestClass)
	{
	public:

		TEST_METHOD(PrintingTest)
		{
			//Arrange
			Piece piece{ Piece::Color::BROWN, 
						Piece::Height::SHORT, 
						Piece::Shape::ROUND, 
						Piece::Body::HOLLOW };

			std::stringstream stream;
			std::string expectedResult = "1011";

			//Act
			stream << piece;
			std::string result = stream.str();

			//Assert
			Assert::AreEqual(expectedResult, result);
		}
	};
}
