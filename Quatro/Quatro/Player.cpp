#include "Player.h"

Player::Player(const std::string& name)
	:m_name(name) {}

Piece Player::PickPiece(std::istream& stream, UnusedPieces& unusedPieces) const
{
	std::string pieceName;

	stream >> pieceName;

	return unusedPieces.PickPiece(pieceName);
}

Board::Position Player::PlacePiece(std::istream& stream, Piece&& piece, Board& board) const
{
	uint16_t rowIndex, columnIndex;

	if (stream >> rowIndex  && stream >> columnIndex)
	{
		Board::Position position = std::make_pair(static_cast<uint8_t>(rowIndex), static_cast<uint8_t>( columnIndex));

		auto& optionalPiece = board[position];

		if (optionalPiece.has_value())
		{
			throw "there is already one piece on this position";
		}

		optionalPiece = piece;
		return position;
	}

	stream.clear();
	stream.seekg(std::ios::end);

	throw "Please insert numbers between 0 and 3";
}


std::ostream& operator<<(std::ostream& stream, const Player& other)
{
	stream << other.m_name;
	return stream;
}
