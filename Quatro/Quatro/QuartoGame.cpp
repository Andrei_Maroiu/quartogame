#include "QuartoGame.h"

void QuartoGame::start()
{
	std::string name;

	std::cout << "Player1 name is:\n";
	std::cin >> name;
	Player player1(name);

	std::cout << "Player2 name is:\n";
	std::cin >> name;
	Player player2(name);

	UnusedPieces unusedPieces;
	Board board;

	auto pickingPlayer = std::ref(player1);
	auto placingPlayer = std::ref(player2);

	while (true) //main loop
	{
		system("cls");

		std::cout << "The board looks like this:\n";
		std::cout << board << "\n";

		std::cout << "The unused pieces are:\n";
		std::cout << unusedPieces << "\n";

		std::cout << pickingPlayer << " Please choose a piece\n";
		Piece pickedPiece;

		while (true)
		{
			try
			{
				pickedPiece = pickingPlayer.get().PickPiece(std::cin, unusedPieces);
				break;
			}
			catch (const char* exception)
			{
				std::cout << exception << "\n";
			}
		}

		std::cout << placingPlayer << " Please place the piece\n";
		Board::Position position;

		while (true)
		{
			try
			{
				position = placingPlayer.get().PlacePiece(std::cin, std::move(pickedPiece), board);
				break;
			}
			catch (const char* exception)
			{
				std::cout << exception << "\n";
			}
		}

		//verificam daca conditia de castigare este indeplinita

		std::swap(placingPlayer, pickingPlayer);
	}
}
