#include "UnusedPieces.h"

std::ostream& operator<<(std::ostream& stream, const UnusedPieces& other)
{
    for (const auto& [pieceName, piece] : other.m_pool)
    {
        stream << pieceName << " ";
    }

    return stream;
}

void UnusedPieces::EmplacePiece(Piece&& piece)
{
    std::stringstream stream;
    stream << piece;
    m_pool.emplace(stream.str(), std::forward<Piece&&>(piece));
}

UnusedPieces::UnusedPieces()
{
    EmplacePiece(Piece{ Piece::Color::WHITE,Piece::Height::SHORT,Piece::Shape::ROUND,Piece::Body::FULL });
    EmplacePiece(Piece{ Piece::Color::WHITE,Piece::Height::SHORT,Piece::Shape::ROUND,Piece::Body::HOLLOW });
    EmplacePiece(Piece{ Piece::Color::WHITE,Piece::Height::SHORT,Piece::Shape::SQUARE,Piece::Body::FULL });
    EmplacePiece(Piece{ Piece::Color::WHITE,Piece::Height::SHORT,Piece::Shape::SQUARE,Piece::Body::HOLLOW });
    EmplacePiece(Piece{ Piece::Color::BROWN,Piece::Height::SHORT,Piece::Shape::ROUND,Piece::Body::FULL });
    EmplacePiece(Piece{ Piece::Color::BROWN,Piece::Height::SHORT,Piece::Shape::ROUND,Piece::Body::HOLLOW });
    EmplacePiece(Piece{ Piece::Color::BROWN,Piece::Height::SHORT,Piece::Shape::SQUARE,Piece::Body::FULL });
    EmplacePiece(Piece{ Piece::Color::BROWN,Piece::Height::SHORT,Piece::Shape::SQUARE,Piece::Body::HOLLOW });

    EmplacePiece(Piece{ Piece::Color::WHITE,Piece::Height::TALL,Piece::Shape::ROUND,Piece::Body::FULL });
    EmplacePiece(Piece{ Piece::Color::WHITE,Piece::Height::TALL,Piece::Shape::ROUND,Piece::Body::HOLLOW });
    EmplacePiece(Piece{ Piece::Color::WHITE,Piece::Height::TALL,Piece::Shape::SQUARE,Piece::Body::FULL });
    EmplacePiece(Piece{ Piece::Color::WHITE,Piece::Height::TALL,Piece::Shape::SQUARE,Piece::Body::HOLLOW });
    EmplacePiece(Piece{ Piece::Color::BROWN,Piece::Height::TALL,Piece::Shape::ROUND,Piece::Body::FULL });
    EmplacePiece(Piece{ Piece::Color::BROWN,Piece::Height::TALL,Piece::Shape::ROUND,Piece::Body::HOLLOW });
    EmplacePiece(Piece{ Piece::Color::BROWN,Piece::Height::TALL,Piece::Shape::SQUARE,Piece::Body::FULL });
    EmplacePiece(Piece{ Piece::Color::BROWN,Piece::Height::TALL,Piece::Shape::SQUARE,Piece::Body::HOLLOW });
}

Piece UnusedPieces::PickPiece(const std::string& name)
{
    /*auto pieceIterator = m_pool.find(name);

    if (pieceIterator != m_pool.end())
    {
        Piece temp = std::move(pieceIterator->second);
        m_pool.erase(name);
        return temp;
    }

    throw "Piece not found";*/

    if (auto node = m_pool.extract(name); node)
    {
        return std::move(node.mapped());
    }

    throw "Piece not found";
}
