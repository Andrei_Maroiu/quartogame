#include "Piece.h"

Piece::Piece()
	:Piece(Piece::Color::NONE, Piece::Height::NONE, Piece::Shape::NONE, Piece::Body::NONE) {}

Piece::Piece(Color color, Height height, Shape shape, Body body)
	:m_color{color}, m_height {height}, m_shape{shape}, m_body {body}
{
	static_assert(sizeof(*this) == 1, "Size greater than 1");
}

Piece::Piece(const Piece& other)
{
	*this = other;
}

Piece::Piece(Piece&& other) noexcept
{
	*this = std::move(other);
}

Piece::~Piece()
{
	m_color = Piece::Color::NONE;
	m_body = Piece::Body::NONE;
	m_shape = Piece::Shape::NONE;
	m_height = Piece::Height::NONE;
}

Piece& Piece::operator=(const Piece& other)
{
	m_color = other.m_color;
	m_body = other.m_body;
	m_shape = other.m_shape;
	m_height = other.m_height;

	return *this;
}

Piece& Piece::operator=(Piece&& other) noexcept
{
	m_color = other.m_color;
	m_body = other.m_body;
	m_shape = other.m_shape;
	m_height = other.m_height;

	new(&other) Piece();

	return *this;
}

Piece::Color Piece::GetColor() const noexcept
{
	return m_color;
}

Piece::Height Piece::GetHeight() const noexcept
{
	return m_height;
}

Piece::Shape Piece::GetShape() const noexcept
{
	return m_shape;
}

Piece::Body Piece::GetBody() const noexcept
{
	return m_body;
}

Piece& Piece::operator&=(const Piece& other)
{
	if (m_color != other.m_color)
	{
		m_color = Color::NONE;
	}

	if (m_body != other.m_body)
	{
		m_body = Body::NONE;
	}

	if (m_shape != other.m_shape)
	{
		m_shape = Shape::NONE;
	}

	if (m_height != other.m_height)
	{
		m_height = Height::NONE;
	}

	return *this;
}

bool Piece::HasAnyFeature() const
{
	return m_color != Color::NONE || m_body != Body::NONE || m_height != Height::NONE || m_shape != Shape::NONE;
}

Piece operator&(const Piece& first, const Piece& second)
{
	return Piece(first) &= second;
}

std::ostream& operator<<(std::ostream& stream, const Piece& other)
{
	stream << static_cast<int>(other.m_color) - 1
		<< static_cast<int>(other.m_height) - 1
		<< static_cast<int>(other.m_shape) - 1
		<< static_cast<int>(other.m_body) - 1;

	return stream;
}
