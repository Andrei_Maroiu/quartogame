#include "Board.h"

std::optional<Piece>& Board::operator[](const Position& position)
{
	const auto& [rowIndex, columnIndex] = position;

	if (rowIndex < m_height && columnIndex < m_width)
	{
		return m_board[rowIndex * Board::m_height + columnIndex];
	}

	throw "index out of range!";
}

const std::optional<Piece>& Board::operator[](const Position& position) const
{
	const auto& [rowIndex, columnIndex] = position;

	if (rowIndex < m_height && columnIndex < m_width)
	{
		return m_board[rowIndex * Board::m_height + columnIndex];
	}

	throw "index out of range!";
}

std::pair<Board::RowIterator, Board::RowIterator> Board::GetRow(const int index)
{
	return
	{
		RowIterator(m_board, index * m_width), RowIterator(m_board, (index + 1) * m_width)
	};
}

std::pair<Board::ColumnIterator, Board::ColumnIterator> Board::GetColumn(const int index)
{
	return
	{
		ColumnIterator(m_board, index), ColumnIterator(m_board, m_size + index)
	};
}

std::pair<Board::MainDiagonalIterator, Board::MainDiagonalIterator> Board::GetMainDiagonal()
{
	return
	{
		MainDiagonalIterator(m_board, 0), MainDiagonalIterator(m_board, m_size + m_width)
	};
}

std::pair<Board::SecondaryDiagonalIterator, Board::SecondaryDiagonalIterator> Board::GetSecondDiagonal()
{
	return
	{
		SecondaryDiagonalIterator(m_board, m_height - 1), SecondaryDiagonalIterator(m_board,  m_size - 1)
	};
}

const char emptyBoardCell[] = "____";

std::ostream& operator<<(std::ostream& stream, const Board& board)
{
	Board::Position pos;
	auto& [row, column] = pos;

	for (row = 0; row < Board::m_height; ++row)
	{
		for (column = 0; column < Board::m_width; ++column)
		{
			if (auto& piece = board[pos]; piece)
			{
				stream << piece.value() << " ";
			}
			else
			{
				stream << emptyBoardCell << " ";
			}
		}
		stream << "\n";
	}

	return stream;
}

const std::optional<Piece> Board::BaseIterator::empty = Piece();

Board::BaseIterator::BaseIterator(std::array<std::optional<Piece>, m_size>& data, int index)
	: m_data{ data }, m_index{ index }
{
}

const std::optional<Piece>& Board::BaseIterator::operator*()
{
	assert(m_index < m_size);
	
	return m_data[m_index] ? m_data[m_index] : empty;
}

bool Board::BaseIterator::operator!=(const BaseIterator& other) const noexcept
{
	return m_index != other.m_index;
}

const std::optional<Piece>* Board::BaseIterator::operator->()
{
	return &*(*this);
}

//Board::RowIterator::RowIterator(std::array<std::optional<Piece>, m_size>& data, int index)
//	: BaseIterator(data, index)
//{
//
//}

Board::RowIterator& Board::RowIterator::operator++()
{
	++m_index;

	return *this;
}

Board::RowIterator Board::RowIterator::operator++(int)
{
	RowIterator temp{ *this };

	++* this;

	return temp;
}

Board::ColumnIterator& Board::ColumnIterator::operator++()
{
	m_index += m_width;

	return *this;
}

Board::ColumnIterator Board::ColumnIterator::operator++(int)
{
	ColumnIterator temp{ *this };

	++* this;

	return temp;
}

Board::MainDiagonalIterator& Board::MainDiagonalIterator::operator++()
{
	m_index += (m_width + 1);

	return *this;
}

Board::MainDiagonalIterator Board::MainDiagonalIterator::operator++(int)
{
	MainDiagonalIterator temp{ *this };

	++temp;

	return temp;
}

Board::SecondaryDiagonalIterator& Board::SecondaryDiagonalIterator::operator++()
{
	m_index += (m_width - 1);

	return *this;
}

Board::SecondaryDiagonalIterator Board::SecondaryDiagonalIterator::operator++(int)
{
	SecondaryDiagonalIterator temp{ *this };

	++* this;

	return temp;
}
