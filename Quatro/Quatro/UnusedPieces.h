#pragma once
#include "Piece.h"
#include <unordered_map>
#include <iostream>
#include <sstream>

class UnusedPieces
{
private:
	std::unordered_map<std::string, Piece> m_pool;

	void EmplacePiece(Piece&& piece);

public:
	UnusedPieces();

	Piece PickPiece(const std::string& name);

	friend std::ostream& operator << (std::ostream& stream, const UnusedPieces& other);
};

