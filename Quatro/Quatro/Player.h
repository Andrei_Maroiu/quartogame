#pragma once
#include <string>
#include <iostream>
#include "Board.h"
#include "UnusedPieces.h"

class Player
{
private:
	std::string m_name;

public:
	Player(const std::string& name);

	Piece PickPiece(std::istream& stream, UnusedPieces& unusedPieces) const;
	Board::Position PlacePiece(std::istream& stream, Piece&& piece, Board& board) const;

	friend std::ostream& operator<<(std::ostream& stream, const Player& other);
};

