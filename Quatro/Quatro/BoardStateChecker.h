#pragma once

#include "Board.h"
#include <algorithm>
#include <numeric>

class BoardStateChecker
{
public:

	enum class State
	{
		UNFINISHED,
		EQUAL,
		WIN
	};

	State CheckBoard(Board& board, const Board::Position& position);
};

