// Quatro.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Piece.h"
#include "Board.h"
#include "UnusedPieces.h"
#include "Player.h"
#include "QuartoGame.h"
#include "../LoggerDLL/Logger.h"

int main()
{
    Logger logger {std::cout};

    //Piece piece(Piece::Color::BROWN, Piece::Height::SHORT, Piece::Shape::SQUARE, Piece::Body::FULL);
    //std::cout << "Full, Dark, Short, Square piece: " << piece << std::endl;

    //Board board;
    //std::cout << "Empty board:\n" << board << std::endl;

    //board[{0, 0}] = std::move(piece);
    //std::cout << "Moved piece to board:\n" << board << std::endl;

    //UnusedPieces unusedPieces;
    //std::cout << "All available pieces:\n" << unusedPieces << std::endl;
    //unusedPieces.PickPiece("0001");
    //std::cout << "Extracted \"0001\" remaining pieces after extracted:\n" << unusedPieces << std::endl;

    //Player player("Player_1");
    //Piece pickedPiece = player.PickPiece(std::cin, unusedPieces);
    //std::cout << "Picked piece: " << pickedPiece << std::endl;
    //std::cout << "Remaining pieces:\n" << unusedPieces << std::endl;
    //const auto& [line, column] = player.PlacePiece(std::cin, std::move(pickedPiece), board);
    //std::cout << "Board after placing piece on position ("
    //    << static_cast<uint16_t>(line) << "," << static_cast<uint16_t>(column) << "):\n" << board;

    QuartoGame game;
    game.start();

    logger.log("Main runned succefully.");
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
