#include "BoardStateChecker.h"

BoardStateChecker::State BoardStateChecker::CheckBoard(Board& board, const Board::Position& position)
{
    const auto& [lineIndex, columnIndex] = position;

    //Board& board2 = const_cast<Board&> (board);

    auto [first, last] = board.GetRow(lineIndex);

    Piece piece = std::accumulate(first, last, (first++)->value(),
        [](const Piece& piece, const std::optional<Piece>& optionalPiece) 
        {
            return piece & (optionalPiece.value()); 
        }
    );

    if (piece.HasAnyFeature())
    {
        return State::WIN;
    }
}
