#pragma once
#include "Piece.h"
#include <array>
#include <optional>
#include <cassert>

class Board
{
private:
	const static int m_height = 4;
	const static int m_width = 4;
	const static int m_size = m_height * m_width;

	std::array<std::optional<Piece>, m_size> m_board;

public:

	class BaseIterator
	{
	public:
		BaseIterator(std::array<std::optional<Piece>, m_size>& data, int index);

		const std::optional<Piece>& operator*();
		bool operator != (const BaseIterator& other) const noexcept;
		const std::optional<Piece>* operator->();

	protected:
		std::array<std::optional<Piece>, m_size>& m_data;
		int m_index;

	private:
		const static std::optional<Piece> empty;
	};

	class RowIterator : public BaseIterator
	{
	public:
		//RowIterator(std::array<std::optional<Piece>, m_size>& data, int index);

		using BaseIterator::BaseIterator;

		RowIterator& operator++();
		RowIterator operator++(int);
	};

	class ColumnIterator : public BaseIterator
	{
	public:

		using BaseIterator::BaseIterator;

		ColumnIterator& operator++();
		ColumnIterator operator++(int);
	};

	class MainDiagonalIterator : public BaseIterator
	{
	public:

		using BaseIterator::BaseIterator;

		MainDiagonalIterator& operator++();
		MainDiagonalIterator operator++(int);
	};

	class SecondaryDiagonalIterator : public BaseIterator
	{
	public:

		using BaseIterator::BaseIterator;

		SecondaryDiagonalIterator& operator++();
		SecondaryDiagonalIterator operator++(int);
	};

	using Position = std::pair<uint8_t, uint8_t>;

	std::optional<Piece>& operator[] (const Position& position);
	const std::optional<Piece>& operator[] (const Position& position) const;

	friend std::ostream& operator<<(std::ostream& stream, const Board& board);

	std::pair<RowIterator, RowIterator> GetRow(const int index);
	std::pair<ColumnIterator, ColumnIterator> GetColumn(const int index);
	std::pair<MainDiagonalIterator, MainDiagonalIterator> GetMainDiagonal();
	std::pair<SecondaryDiagonalIterator, SecondaryDiagonalIterator> GetSecondDiagonal();
};

