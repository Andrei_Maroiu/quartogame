#pragma once
#include <cstdint>
#include <iostream>

class Piece
{
public:
	enum class Color : uint8_t
	{
		NONE,
		WHITE,
		BROWN
	};

	enum class Height : uint8_t
	{
		NONE,
		SHORT,
		TALL
	};

	enum class Shape : uint8_t
	{
		NONE,
		SQUARE,
		ROUND
	};

	enum class Body : uint8_t
	{
		NONE,
		FULL,
		HOLLOW
	};

public:
	Piece();
	Piece(Color color, Height height, Shape shape, Body body);
	Piece(const Piece& other);
	Piece(Piece&& other) noexcept;
	~Piece();

	Piece& operator=(const Piece& other);
	Piece& operator=(Piece&& other) noexcept;

	Color GetColor() const noexcept;
	Height GetHeight() const noexcept;
	Shape GetShape() const noexcept;
	Body GetBody() const noexcept;


	Piece& operator&=(const Piece& other);
	friend Piece operator& (const Piece& first, const Piece& second);

	bool HasAnyFeature() const;

	friend std::ostream& operator <<(std::ostream& stream, const Piece& other);

private:
	Color m_color : 2;
	Height m_height : 2;
	Shape m_shape : 2;
	Body m_body : 2;
};

