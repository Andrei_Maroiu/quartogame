#pragma once
#include <iostream>

#ifdef LOGGERDLL_EXPORTS
#define LOGGING_API __declspec(dllexport)
#else
# define LOGGING_API __declspec(dllimport)
#endif

class LOGGING_API Logger
{
public:
	enum class Level
	{
		INFO,
		ERROR,
		WARNING,

		COUNT
	};

	Logger(std::ostream& outstream, Level minimumLevel = Level::INFO);

	void log(const std::string& message, Level level = Level::INFO);
	void log(const char* message, Level level = Level::INFO);

private:
	std::ostream& m_out;
	Level m_minimumLevel;
};

std::string LoggerLeverToString(Logger::Level level);