#include "Logger.h"

Logger::Logger(std::ostream& outstream, Level minimumLevel)
	:m_out {outstream}, m_minimumLevel {minimumLevel}
{}

void Logger::log(const std::string & message, Level level)
{
	log(message.c_str(), level);
}

void Logger::log(const char* message, Level level)
{
	if (static_cast<int>(level) < static_cast<int>(m_minimumLevel))
	{
		return;
	}
	if (static_cast<int>(level) >= static_cast<int>(Level::COUNT))
	{
		return;
	}

	m_out << "[" << LoggerLeverToString(level) << "] " << message << "\n";
}

std::string LoggerLeverToString(Logger::Level level)
{
	switch (level)
	{
	case Logger::Level::INFO:
		return "Info";
	case Logger::Level::ERROR:
		return "Error";
	case Logger::Level::WARNING:
		return "Warning";
	case Logger::Level::COUNT:
	default:
		return "";
	}
}
